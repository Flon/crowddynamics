#pragma once
#include <Math.h>
#include <array>
#include <random>
#include <time.h>
#include "vec2d.h"

class boid
{
public:
	boid();
	~boid();
	vec2d vel;
	vec2d pos;
	vec2d velPrev;
	vec2d posPrev;
};

