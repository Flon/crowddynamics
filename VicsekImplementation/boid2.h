#pragma once
#include <Math.h>
#include <array>
#include <random>
#include <time.h>
#include "vec2d.h"

class boid2
{
public:
	boid2();
	~boid2();
	vec2d psi;
	vec2d psiPrev;
	vec2d pos;
	vec2d posPrev;
	vec2d F;
};
