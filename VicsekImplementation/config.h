#pragma once
class config
{
public:
	config();
	~config();

	// Model
	unsigned int N = 1000;
	unsigned int gridSizeX = 33;
	unsigned int gridSizeY = 33;
	float speed = 1.f;
	float range = 1.5f;
	float noiseMagnitude = 0.20f;
	float dt = 0.01f;
	float tao = 1.f;
	float K = 1;

	// Rendering
	bool drawTrace = false;
	bool focusView = false;
	bool drawRadius = false;
	bool drawDebugGrid = false;
	bool drawDebugTris = false;
	bool drawDensity = true;
	bool drawDensitySquares = false;
	bool drawHelper = true;
	bool drawBoxes = true;
	bool drawPressure = true;
};