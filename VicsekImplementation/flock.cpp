#include "flock.h"
#include "space.h"

flock::flock(space* par)
{
	pSpace = par;
	N = pSpace->cfg->N;
	gridSizeX = pSpace->cfg->gridSizeX;
	gridSizeY = pSpace->cfg->gridSizeY;
	range = pSpace->cfg->range;
	speed = pSpace->cfg->speed;
	noiseMagnitude = pSpace->cfg->noiseMagnitude;

	//vBoxes = new std::vector<std::vector<boid*>*>[5];
	// create partition grid
	grid = std::vector<std::vector<boid*>>(gridSizeX*gridSizeY, std::vector<boid*>(10));
	//grid.reserve(gridSizeX*gridSizeY*10);
	for (unsigned int i = 0; i < gridSizeX*gridSizeY; i++) grid[i].reserve(60);
	//for (unsigned int i = 0; i < 5; i++) vBoxes[i].reserve(60);
	boids = std::vector<boid>(N);
}


flock::~flock()
{
}

void flock::randomise() 
{
	// seed the random generator
	srand(static_cast<unsigned int>(time(NULL))); // not sure if this does anything now due to rd!!

	std::random_device rd;
	std::mt19937 gen(rd());

	std::uniform_real_distribution<float> dis(0.f, 100.f);
	std::uniform_real_distribution<float> disAng(0.f, 6.28f);
	// apply random postions and velocities
	float dir = 0.f;
	for (unsigned int i = 0; i < N; i++)
	{
		boids[i].pos.x = dis(gen);//((float)rand() / (RAND_MAX))*100.f;
		boids[i].pos.y = dis(gen);//((float)rand() / (RAND_MAX))*100.f;

		dir = disAng(gen);//2.f * 3.14f *((float)rand() / (RAND_MAX));
		boids[i].vel.x = std::cos(dir);
		boids[i].vel.y = std::sin(dir);
	}
}

void flock::update()
{
	std::cout << " orderParam: " << pSpace->stats->calcOrderParam() << "\n";
	// store previous positions and velocities
	for (unsigned int i = 0; i < N; i++)
	{
		boids[i].posPrev = boids[i].pos;
		boids[i].velPrev = boids[i].vel;
		// clear current velocities for recalculation
		boids[i].vel.x = 0.f;
		boids[i].vel.y = 0.f;
	}
	// update velocities
	calcOrientP();
	// update positions
	for (unsigned int i = 0; i < N; i++)
	{
		boids[i].pos.x += speed*boids[i].vel.x;
		boids[i].pos.y += speed*boids[i].vel.y;

		// wrap around
		if (boids[i].pos.x > 100.f)
			boids[i].pos.x -= 100.f;
		else if (boids[i].pos.x < 0.f)
			boids[i].pos.x += 100.f;
		if (boids[i].pos.y > 100.f)
			boids[i].pos.y -= 100.f;
		else if (boids[i].pos.y < 0.f)
			boids[i].pos.y += 100.f;
	}
}

void flock::calcOrientBF()
{
	//1. Bruteforce
	//find birds in range
	for (unsigned int i = 0; i < gridSizeX; i++)
	{
		for (unsigned int j = 0; j < gridSizeY; j++)
		{
			// if boid in range add to velocity vector
			
			
			// normalise velocity vector


		}
	}
	
}

void flock::calcOrientP()
{
	// setup number generator for theta
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<float> dis(0.f, noiseMagnitude);

	// insert boids into partition
	partition();
	// create vector for relevant grid boxes
	std::vector<boid*>* vBoxes[5];
	for (unsigned int gy = 0; gy < gridSizeY; gy++)
	{
		for (unsigned int gx = 0; gx < gridSizeX; gx++)
		{	
			// add relevant grid boxes
			// add current box
			vBoxes[0] = &grid[gridSizeX*gy + gx];
			if ((gy + 1) < gridSizeY)
			{
				// add top box
				vBoxes[1] = &grid[gridSizeX*(gy + 1) + gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gx - 1)];
				else
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gridSizeX - 1)];
				// add top right box
				if ( gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gridSizeX*(gy + 1) + (gx + 1)];
				else
					vBoxes[3] = &grid[gridSizeX*(gy + 1)];
			}
			else
			{
				// add top box
				vBoxes[1] = &grid[gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gx - 1];
				else
					vBoxes[2] = &grid[gridSizeX - 1];
				// add top right box
				if (gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gx + 1];
				else
					vBoxes[3] = &grid[0];
			}
			// add right box
			if (gx + 1 < gridSizeX)
				vBoxes[4] = &grid[gridSizeX*gy + (gx + 1)];
			else
				vBoxes[4] = &grid[gridSizeX*gy];

			boid* b0 = nullptr;
			boid* b1 = nullptr;
			// run bruteforce on each box
			for (unsigned int i = 0; i < (*vBoxes[0]).size(); i++)
			{
				// add box (gx,gy) boid velocities to carry forward to current velocity
				b0 = (*vBoxes[0])[i];

				b0->vel.x += b0->velPrev.x;
				b0->vel.y += b0->velPrev.y;
				// process each surrounding box
				for (unsigned int j = 1; j < 5; j++)
				{
					for (unsigned int k = 0; k < (*vBoxes[j]).size(); k++)
					{
						b1 = (*vBoxes[j])[k];
						// if boid in range add velocity vectors
						if (distSq(b0, b1) < range*range)
						{
							b0->vel.x += b1->velPrev.x;
							b0->vel.y += b1->velPrev.y;
							b1->vel.x += b0->velPrev.x;
							b1->vel.y += b0->velPrev.y;
						}
					}
				}
				
				
				// add noise (rotate vector by random angle)
				float theta = dis(gen);
				float x = b0->vel.x;
				float y = b0->vel.y;
						
				// matrix rotation
				b0->vel.x = x*cos(theta) - y*sin(theta);
				b0->vel.y = x*sin(theta) + y*cos(theta);
				x = b0->vel.x;
				y = b0->vel.y;

				// normalise velocity vector
				//float mag = sqrt(x*x + y*y);
				//b0->vel.x /= mag;
				//b0->vel.y /= mag;
				
			}
		}
	}
	normaliseFlock();
}

void flock::partition()
{
	//insert boids
	for (unsigned int i = 0; i < grid.size(); i++)
	{
		grid[i].clear();
	}

	for (unsigned int i = 0; i < N; i++)
	{
		//round down position
		unsigned int gridX;
		unsigned int gridY;
		gridX = (unsigned int)(boids[i].pos.x*(float)(gridSizeX) / 100.f);// / (float)gridSizeX);
		gridY = (unsigned int)(boids[i].pos.y*(float)(gridSizeY) / 100.f);// / (float)gridSizeY);
		if (gridX >= gridSizeX) gridX = gridSizeX - 1; // hacky
		if (gridY >= gridSizeY) gridY = gridSizeY - 1; // hacky
		grid[gridSizeX*gridY + gridX].push_back(&boids[i]); //using 2d to 1d array formula
	}
}

inline void flock::normaliseFlock()
{
	for (unsigned int i = 0; i < N; i++)
	{
		// normalise velocity vector
		float x = boids[i].vel.x;
		float y = boids[i].vel.y;
		float mag = sqrt(x*x + y*y);
		boids[i].vel.x /= mag;
		boids[i].vel.y /= mag;
	}
}

float flock::distSq(const boid* b0, const boid* b1)
{
	count += 1;
	float distSq = 0.f;
	
	float delta;
	delta = b0->pos.x - b1->pos.x;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	delta = b0->pos.y - b1->pos.y;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	return distSq;
}