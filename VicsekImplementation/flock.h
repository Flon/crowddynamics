#pragma once
#include "boid.h"
#include <array>
#include <list>
#include <iostream>
#include "geom.h"
class space;

class flock
{
public:
	std::vector<boid> boids;

	flock(space* par);
	~flock();
	void randomise();
	void update();
	unsigned int N;
	float range;

private:
	unsigned int gridSizeX;
	unsigned int gridSizeY;
	float speed;
	float noiseMagnitude;
	bool drawTrace;
	space* pSpace = nullptr;
	std::vector<std::vector<boid*>> grid;

	void partition();
	//Bruteforce
	void calcOrientBF();
	//Partitioned
	void calcOrientP();

	void checkGrid(unsigned int box0X, unsigned int box0Y, unsigned int box1X, unsigned int box1Y, bool xBound, bool yBound);
	void addNoise();
	void normaliseFlock();
	bool inRange(boid* b0, boid* b1, float range);
	float distSq(const boid* b0, const boid* b1);
	//std::vector<std::vector<boid*>*>* vBoxes;
	int count = 0;
	// random generators
};