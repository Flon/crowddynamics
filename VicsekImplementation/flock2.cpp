#include "flock2.h"
#include "space.h"
#include "rasterise.h"
#include "geometry.h"
flock2::flock2(space* par, geometry& geom) : walls(geom.walls)
{
	pSpace = par;
	N = pSpace->cfg->N;
	gridSizeX = pSpace->cfg->gridSizeX;
	gridSizeY = pSpace->cfg->gridSizeY;
	range = pSpace->cfg->range;
	speed = pSpace->cfg->speed;
	tao = pSpace->cfg->tao;
	dt = pSpace->cfg->dt;
	K = pSpace->cfg->K;
	noiseMagnitude = pSpace->cfg->noiseMagnitude;

	raster = new rasteriser(this);
	//vBoxes = new std::vector<std::vector<boid*>*>[5];
	// create partition grid
	grid = std::vector<std::vector<boid2*>>(gridSizeX*gridSizeY, std::vector<boid2*>(20));
	wallIntersections = std::vector<std::vector<std::vector<unsigned int>>>(0, std::vector<std::vector<unsigned int>>(0, std::vector<unsigned int>(0)));
	pressures = std::vector<float>(gridSizeX*gridSizeY);
	//grid.reserve(gridSizeX*gridSizeY*10);
	for (unsigned int i = 0; i < gridSizeX*gridSizeY; i++) grid[i].reserve(60);
	//for (unsigned int i = 0; i < 5; i++) vBoxes[i].reserve(60);
	boids = std::vector<boid2>(N);
	if (walls.size() > 0)
		addWalls();
}


flock2::~flock2()
{
}

void flock2::randomise()
{
	std::vector<line>& spawnBoxes = pSpace->geom->spawnBoxes;
	int numBoxes = spawnBoxes.size();
	

	if (numBoxes > 0)
	{
		int n = (int)(float)N / numBoxes;
		for (int i = 0; i < numBoxes; i++)
		{
			int n1 = n*i;
			int n2 = n*(i+1);
			randomiseSquare(spawnBoxes[i].A, spawnBoxes[i].B, n1, n2);
		}
	}
	else
	{
		vec2d A,B;
		A.x = 0.f;
		A.y = 0.f;
		B.x = 100.f;
		B.y = 100.f;
		randomiseSquare(A, B, 0, N);
	}
}

void flock2::randomiseSquare(vec2d A, vec2d B, int n1, int n2)
{
	// seed the random generator
	srand(static_cast<unsigned int>(time(NULL))); // not sure if this does anything now due to rd!!

	std::random_device rd;
	std::mt19937 gen(rd());

	float minX = A.x;
	float maxX = B.x;
	float minY = A.y;
	float maxY = B.y;
	
	std::uniform_real_distribution<float> distributionX(minX, maxX);
	std::uniform_real_distribution<float> distributionY(minY, maxY);
	std::uniform_real_distribution<float> distributionAng(0.f, 6.28f);
	// apply random postions and velocities
	float dir = 0.f;
	for (unsigned int i = n1; i < n2; i++)
	{
		boids[i].pos.x = distributionX(gen);//((float)rand() / (RAND_MAX))*100.f;
		boids[i].pos.y = distributionY(gen);//((float)rand() / (RAND_MAX))*100.f;

		dir = distributionAng(gen);//2.f * 3.14f *((float)rand() / (RAND_MAX));
		boids[i].psi.x = std::cos(dir);
		boids[i].psi.y = std::sin(dir);
	}
}

void flock2::update()
{
	// setup number generator for theta
	std::random_device rd;
	std::mt19937 gen(rd());
	//std::cout << " orderParam: " << pSpace->stats->calcOrderParam() << "\n";
	// store previous positions and velocities
	for (unsigned int i = 0; i < N; i++)
	{
		boids[i].posPrev = boids[i].pos;
		boids[i].psiPrev = boids[i].psi;
		// clear current velocities for recalculation
		boids[i].F.x = 0.f;
		boids[i].F.y = 0.f;
	}
	// update velocities
	calcForces();
	if (true)
		attract();
	// update positions
	std::normal_distribution<float> dis(0.f, noiseMagnitude*sqrt(dt));
	float mu, v0, theta, mag;
	vec2d vi, torque;
	mu = 1.f;
	v0 = speed;
	for (unsigned int i = 0; i < N; i++)
	{
		vi.x = mu*boids[i].F.x + boids[i].psi.x*v0;
		vi.y = mu*boids[i].F.y + boids[i].psi.y*v0;
		boids[i].pos.x += dt*vi.x;
		boids[i].pos.y += dt*vi.y;
		// update psi
		// calculate theta
		// noise
		
		theta = dis(gen);
		//float x = boids[i].psi.x;
		//float y = boids[i].psi.y;
		float vmag = sqrt(vi.x*vi.x + vi.y*vi.y);

		torque.x = -((boids[i].psi.x*vi.y - vi.x*boids[i].psi.y)*dt/(vmag*tao) + theta)*boids[i].psi.y;
		torque.y = ((boids[i].psi.x*vi.y - vi.x*boids[i].psi.y)*dt/(vmag*tao) + theta)*boids[i].psi.x;
		boids[i].psi.x = boids[i].psi.x + torque.x;
		boids[i].psi.y = boids[i].psi.y + torque.y;
		// normalise psi (precision errors)
		mag = sqrt(boids[i].psi.x*boids[i].psi.x + boids[i].psi.y*boids[i].psi.y);
		//std::cout << mag << "\n";
		boids[i].psi.x /= mag;
		boids[i].psi.y /= mag;

		//-----
		/*
		vec2d thetai;
		float mag = sqrt(vi.x*vi.x + vi.y*vi.y);
		thetai.x = vi.x/mag;
		thetai.y = vi.y/mag;
		// calculate theta - psi
		vec2d dThetaPsi;
		dThetaPsi.x = thetai.x + boids[i].psi.x;
		dThetaPsi.y = thetai.y + boids[i].psi.y;
		mag = sqrt(dThetaPsi.x*dThetaPsi.x + dThetaPsi.y*dThetaPsi.y);
		dThetaPsi.x = dThetaPsi.x / mag;
		dThetaPsi.y = dThetaPsi.y / mag;
		// calculate psi
		float dt = 0.1f; //temporary
		boids[i].psi.x = boids[i].psi.x + dt*(1 / tao)*(dThetaPsi.x);
		boids[i].psi.y = boids[i].psi.y + dt*(1 / tao)*(dThetaPsi.y);
		// add noise (rotate vector by random angle)
		//
		std::normal_distribution<float> dis(0.f, noiseMagnitude*sqrt(dt));
		float theta = dis(gen);
		float x = boids[i].psi.x;
		float y = boids[i].psi.y;
		//
		// matrix rotation
		boids[i].psi.x = x*cos(theta) - y*sin(theta);
		boids[i].psi.y = x*sin(theta) + y*cos(theta);
		*/
		// wrap around
		
		if (boids[i].pos.x > 100.f)
			boids[i].pos.x -= 100.f;
		else if (boids[i].pos.x < 0.f)
			boids[i].pos.x += 100.f;
		if (boids[i].pos.y > 100.f)
			boids[i].pos.y -= 100.f;
		else if (boids[i].pos.y < 0.f)
			boids[i].pos.y += 100.f;
		
	}
}


void flock2::calcForces()
{
	// insert boids into partition
	partition();
	// create vector for relevant grid boxes
	std::vector<boid2*>* vBoxes[5];
	for (unsigned int gy = 0; gy < gridSizeY; gy++)
	{
		for (unsigned int gx = 0; gx < gridSizeX; gx++)
		{
			// add relevant grid boxes
			// add current box
			vBoxes[0] = &grid[gridSizeX*gy + gx];
			if ((gy + 1) < gridSizeY)
			{
				// add top box
				vBoxes[1] = &grid[gridSizeX*(gy + 1) + gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gx - 1)];
				else
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gridSizeX - 1)];
				// add top right box
				if (gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gridSizeX*(gy + 1) + (gx + 1)];
				else
					vBoxes[3] = &grid[gridSizeX*(gy + 1)];
			}
			else
			{
				// add top box
				vBoxes[1] = &grid[gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gx - 1];
				else
					vBoxes[2] = &grid[gridSizeX - 1];
				// add top right box
				if (gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gx + 1];
				else
					vBoxes[3] = &grid[0];
			}
			// add right box
			if (gx + 1 < gridSizeX)
				vBoxes[4] = &grid[gridSizeX*gy + (gx + 1)];
			else
				vBoxes[4] = &grid[gridSizeX*gy];

			boid2* b0 = nullptr;
			boid2* b1 = nullptr;
			// run bruteforce on each box
			for (unsigned int i = 0; i < (*vBoxes[0]).size(); i++)
			{
				// add box (gx,gy) boid velocities to carry forward to current velocity
				b0 = (*vBoxes[0])[i];
				vec2d dir;
				float distance = 0.f;
				//b0->vel.x += b0->velPrev.x; //!!!!!!!!!!
				//b0->vel.y += b0->velPrev.y;
				// process each box
				for (unsigned int j = 0; j < 5; j++)
				{
					for (unsigned int k = 0; k < (*vBoxes[j]).size(); k++)
					{
						b1 = (*vBoxes[j])[k];
						// if boid in range add velocity vectors
						if (b0 != b1)
						{
							if (geom::getDirVec(b0->pos, b1->pos, dir, distance, 2*range))
							{
								// F = k(r-2a)
								//float K = 5.f;
								//float mu = 1.f;
								float mag = K*(2 * range - distance);
								//std::cout << distance << "\n";
								//vec2d Fdir = geom::getDirVec(b0->pos, b1->pos);
								dir.x *= mag;
								dir.y *= mag;
								//b0->F = Fdir;
								b0->F.x -= dir.x;
								b0->F.y -= dir.y;
								// avoid processing same box twice
								if (j != 0)
								{
									b1->F.x += dir.x;
									b1->F.y += dir.y;
								}
								/*
								b0->vel.x += b1->velPrev.x;
								b0->vel.y += b1->velPrev.y;
								b1->vel.x += b0->velPrev.x;
								b1->vel.y += b0->velPrev.y;
								*/
							}
							
						}
					}
				}
				// add noise (rotate vector by random angle)
				//
				//float theta = dis(gen);
				//float x = b0->psi.x;
				//float y = b0->psi.y;
				//
				// matrix rotation
				//b0->psi.x = x*cos(theta) - y*sin(theta);
				//b0->psi.y = x*sin(theta) + y*cos(theta);
				//x = b0->psi.x;
				//y = b0->psi.y;

				// normalise velocity vector
				//float mag = sqrt(x*x + y*y);
				//b0->vel.x /= mag;
				//b0->vel.y /= mag;

			}
		}
	}

	// line collision

	vec2d lineStart, lineEnd;
	/*
	lineStart.x = 50.f;
	lineStart.y = 50.f;
	lineEnd.x = 75.f;
	lineEnd.y = 75.f;
	*/
	vec2d dir;
	float distance = 0.f;
	
	for (unsigned int j = 0; j < walls.size(); j++) // for each wall
	{
		for (unsigned int k = 0; k < wallIntersections[j].size(); k++) // for each affected grid
		{
			int gx = wallIntersections[j][k][0];
			int gy = wallIntersections[j][k][1];
			std::vector<boid2*>& gridCell = grid[gridSizeX*gy + gx];

			for (unsigned int l = 0; l < gridCell.size(); l++) // for each boid in affected grid
			{
				boid2& b0 = *gridCell[l];
				lineStart = walls[j].A;
				lineEnd = walls[j].B;
				if (geom::getDirVecPointLine(b0.pos, lineStart, lineEnd, dir, distance, range))
				{
					// F = k(r-2a)
					// float K = 5.f;
					// float mu = 1.f;
					float mag = 20 * K*(range - distance);

					dir.x *= mag;
					dir.y *= mag;
					b0.F.x += dir.x;
					b0.F.y += dir.y;
				}
				else if (geom::getDirVec(b0.pos, lineStart, dir, distance, range)) // process end points
				{
					float mag = 20 * K*(range - distance);

					dir.x *= mag;
					dir.y *= mag;
					b0.F.x -= dir.x;
					b0.F.y -= dir.y;
				}
				else if (geom::getDirVec(b0.pos, lineEnd, dir, distance, range))
				{
					float mag = 20 * K*(range - distance);

					dir.x *= mag;
					dir.y *= mag;
					b0.F.x -= dir.x;
					b0.F.y -= dir.y;
				}
			}
		}
	}

	// normaliseFlock();
}

void flock2::partition()
{
	//insert boids
	for (unsigned int i = 0; i < grid.size(); i++)
	{
		grid[i].clear();
	}

	for (unsigned int i = 0; i < N; i++)
	{
		//round down position
		unsigned int gridX;
		unsigned int gridY;
		gridX = (unsigned int)(boids[i].pos.x*(float)(gridSizeX) / 100.f);// / (float)gridSizeX);
		gridY = (unsigned int)(boids[i].pos.y*(float)(gridSizeY) / 100.f);// / (float)gridSizeY);
		if (gridX >= gridSizeX) gridX = gridSizeX - 1; // hacky
		if (gridY >= gridSizeY) gridY = gridSizeY - 1; // hacky
		grid[gridSizeX*gridY + gridX].push_back(&boids[i]); //using 2d to 1d array formula
	}
}

inline void flock2::normaliseFlock() // not used in this model
{
	for (unsigned int i = 0; i < N; i++)
	{
		// normalise velocity vector
		float x = boids[i].psi.x;
		float y = boids[i].psi.y;
		float mag = sqrt(x*x + y*y);
		boids[i].psi.x /= mag;
		boids[i].psi.y /= mag;
	}
}

struct deltaVec
{
	vec2d dir;
	float mag;
};

float flock2::dist(const boid2* b0, const boid2* b1)
{
	float distSq = 0.f;

	float delta;
	delta = b0->pos.x - b1->pos.x;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	delta = b0->pos.y - b1->pos.y;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	return sqrt(distSq);
}

vec2d flock2::getDir(const boid2* b0, const boid2* b1)
{
	vec2d dir;
	float delta;
	float distSq = 0.f;
	delta = b0->pos.x - b1->pos.x;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	delta = b0->pos.y - b1->pos.y;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	return dir;
}

void flock2::addWall(line wall)
{
	walls.push_back(wall);
	// partition wall
	raster->partitionWall(wall);
}

void flock2::addWalls()
{
	for (int i = 0; i < walls.size(); i++)
	{
		//walls.push_back(walls[i]);
		// partition wall
		raster->partitionWall(walls[i]);
	}
}

void flock2::attract()
{
	float mag, strength;
	strength = 1.f;
	vec2d vi, torque, fieldDir;

	vec2d& attractor = pSpace->geom->attractor;
	line& attractorBox = pSpace->geom->attractorBox;
	for (unsigned int i = 0; i < N; i++)
	{
		if ((boids[i].pos.x >= std::min(attractorBox.A.x, attractorBox.B.x) && boids[i].pos.x <= std::max(attractorBox.A.x, attractorBox.B.x)) &&
			(boids[i].pos.y >= std::min(attractorBox.A.y, attractorBox.B.y) && boids[i].pos.y <= std::max(attractorBox.A.y, attractorBox.B.y)))
		{
			fieldDir.x = attractor.x - boids[i].pos.x;
			fieldDir.y = attractor.y - boids[i].pos.y;

			float dmag = sqrt(fieldDir.x*fieldDir.x + fieldDir.y*fieldDir.y);

			torque.x = -((boids[i].psi.x*fieldDir.y - fieldDir.x*boids[i].psi.y)*dt / (dmag*strength))*boids[i].psi.y;
			torque.y = ((boids[i].psi.x*fieldDir.y - fieldDir.x*boids[i].psi.y)*dt / (dmag*strength))*boids[i].psi.x;
			boids[i].psi.x = boids[i].psi.x + torque.x;
			boids[i].psi.y = boids[i].psi.y + torque.y;

			// normalise psi (precision errors)
			//mag = sqrt(boids[i].psi.x*boids[i].psi.x + boids[i].psi.y*boids[i].psi.y);
			//std::cout << mag << "\n";
			//boids[i].psi.x /= mag;
			//boids[i].psi.y /= mag;
		}
	}
}

void flock2::calcPressure()
{
	//float boxArea = pow(100.f/gridSizeX, 2);
	// create vector for relevant grid boxes
	std::vector<boid2*>* vBoxes[5];
	for (unsigned int gy = 0; gy < gridSizeY; gy++)
	{
		for (unsigned int gx = 0; gx < gridSizeX; gx++)
		{
			// add relevant grid boxes
			// add current box
			vBoxes[0] = &grid[gridSizeX*gy + gx];
			if ((gy + 1) < gridSizeY)
			{
				// add top box
				vBoxes[1] = &grid[gridSizeX*(gy + 1) + gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gx - 1)];
				else
					vBoxes[2] = &grid[gridSizeX*(gy + 1) + (gridSizeX - 1)];
				// add top right box
				if (gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gridSizeX*(gy + 1) + (gx + 1)];
				else
					vBoxes[3] = &grid[gridSizeX*(gy + 1)];
			}
			else
			{
				// add top box
				vBoxes[1] = &grid[gx];
				// add top left box
				if (gx > 0)
					vBoxes[2] = &grid[gx - 1];
				else
					vBoxes[2] = &grid[gridSizeX - 1];
				// add top right box
				if (gx + 1 < gridSizeX)
					vBoxes[3] = &grid[gx + 1];
				else
					vBoxes[3] = &grid[0];
			}
			// add right box
			if (gx + 1 < gridSizeX)
				vBoxes[4] = &grid[gridSizeX*gy + (gx + 1)];
			else
				vBoxes[4] = &grid[gridSizeX*gy];

			boid2* b0 = nullptr;
			boid2* b1 = nullptr;
			// run bruteforce on each box
			//clear forces 
			pressures[gy*gridSizeX + gx] = 0.f;
			for (unsigned int i = 0; i < (*vBoxes[0]).size(); i++)
			{
				// add box (gx,gy) boid velocities to carry forward to current velocity
				b0 = (*vBoxes[0])[i];
				vec2d dir;
				float distance = 0.f;
				//b0->vel.x += b0->velPrev.x; //!!!!!!!!!!
				//b0->vel.y += b0->velPrev.y;
				// process each box
				for (unsigned int j = 0; j < 5; j++)
				{
					for (unsigned int k = 0; k < (*vBoxes[j]).size(); k++)
					{
						b1 = (*vBoxes[j])[k];
						// if boid in range add Force
						if (b0 != b1)
						{
							if (geom::getDirVec(b0->pos, b1->pos, dir, distance, 2 * range))
							{
								float mag = K*(2 * range - distance);

								pressures[gy*gridSizeX + gx] += mag;// / boxArea;
								// add forces twice for own box
								/*
								if (j == 0)
								{
									pressures[gy*gridSizeX + gx] += mag;// / boxArea;
								}
								*/
							}

						}
					}
				}
			}
		}
	}
}