#pragma once
#include "boid.h"
#include "boid2.h"
#include <array>
#include <list>
#include <iostream>
#include "geom.h"
#include "line.h"
#include "rasterise.h"
#include "geometry.h"

class space;

class flock2
{
public:
	std::vector<boid2> boids;
	std::vector<std::vector<std::vector<unsigned int>>> wallIntersections;
	std::vector < vec2d > debugTriangles; // debugging
	std::vector<line>& walls;
	std::vector<float> pressures;

	rasteriser* raster;

	void addWall(line wall);
	void addWalls();
	flock2(space* par, geometry& geom);
	~flock2();
	void randomise();
	void randomiseSquare(vec2d A, vec2d B, int n1, int n2);
	void update();

	unsigned int N;
	float range;
	unsigned int gridSizeX;
	unsigned int gridSizeY;

	std::vector<std::vector<boid2*>> grid;

	void repartitionWalls(); // for when flock is reset
	void calcPressure();
private:
	float speed;
	float noiseMagnitude;
	bool drawTrace;
	float dt;
	float tao;
	float K;
	space* pSpace = nullptr;
	

	

	void partition();
	void calcForces();

	void checkGrid(unsigned int box0X, unsigned int box0Y, unsigned int box1X, unsigned int box1Y, bool xBound, bool yBound);
	void addNoise();
	void normaliseFlock();
	void attract();
	bool inRange(boid2* b0, boid2* b1, float range);
	float dist(const boid2* b0, const boid2* b1);
	vec2d getDir(const boid2* b0, const boid2* b1);
	//std::vector<std::vector<boid*>*>* vBoxes;
	int count = 0;
	// random generators
};
