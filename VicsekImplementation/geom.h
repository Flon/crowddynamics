#pragma once

#include <math.h>
#include "boid.h"

namespace geom
{
	/*
	float distSqPer(const boid* b0, const boid* b1);
	
	inline float distSq(const boid* b0, const boid* b1)
	{
		float dx = b1->pos.x - b0->pos.x;
		float dy = b1->pos.y - b0->pos.y;
		return dx*dx + dy*dy;
	}
	*/
	inline float distSq(const float x1, const float y1, const float x2, const float y2)
	{
		float dx = x1 - x2;
		float dy = y1 - y2;
		return dx*dx + dy*dy;
	}

	//#define FABS(x) ((x >= 0.f) ? x : -x)
	inline int getDirVec(const vec2d& p0, const vec2d& p1, vec2d& dir, float& distance, const float& range)
	{
		float delta1, delta2 = 0.f;
		if (p0.x < p1.x) // abs()
		{
			delta1 = p1.x - p0.x;
			delta2 = p1.x - p0.x - 100.f;
		}
		else
		{
			delta1 = p1.x - p0.x;
			delta2 = p1.x - p0.x + 100.f;
		}
		if (fabs(delta1) < fabs(delta2))
		{
			dir.x = delta1;
		}
		else
		{
			dir.x = delta2;
		}

		if (p0.y < p1.y) // abs()
		{
			delta1 = p1.y - p0.y;
			delta2 = p1.y - p0.y - 100.f;
		}
		else
		{
			delta1 = p1.y - p0.y;
			delta2 = p1.y - p0.y + 100.f;
		}
		if (fabs(delta1) < fabs(delta2))
		{
			dir.y = delta1;
		}
		else
		{
			dir.y = delta2;
		}
		distance = sqrt(dir.x*dir.x + dir.y*dir.y);
		dir.x /= distance;
		dir.y /= distance;
		if (distance < range)
			return 1;
		return 0;
	}

	inline int getDirVecPointLine(const vec2d& point, vec2d& lineStart, vec2d& lineEnd, vec2d& dir, float& distance, const float& range)
	{
		//lineStart->x = 50.f;
		//lineStart->y = 50.f;
		//lineEnd->x = 75.f;
		//lineEnd->y = 75.f;
		if (lineStart.x == lineEnd.x && lineStart.y == lineEnd.y)
			return 0; // no line defined
		float lineMag;
		float u;
		vec2d intersection;

		vec2d line;
		line.x = lineEnd.x - lineStart.x;
		line.y = lineEnd.y - lineStart.y;
		lineMag = sqrt(line.x*line.x + line.y*line.y);

		u = (((point.x - lineStart.x) * (lineEnd.x - lineStart.x)) +
			((point.y - lineStart.y) * (lineEnd.y - lineStart.y))) /
			(lineMag * lineMag);

		if (u < 0.0f || u > 1.0f)
			return 0;   // closest point does not fall within the line segment

		intersection.x = lineStart.x + u * (lineEnd.x - lineStart.x);
		intersection.y = lineStart.y + u * (lineEnd.y - lineStart.y);
		dir.x = point.x - intersection.x;
		dir.y = point.y - intersection.y;
		distance = sqrt(dir.x*dir.x + dir.y*dir.y);
		dir.x /= distance;
		dir.y /= distance;
		if (distance > range)
			return 0; // points not within range
		return 1;
	}
	//inline vec2d getDirVec(vec2d p0, vec2d p1)
	//{
		//delta = 0.f
		//min(delta, 100 - delta);
	//}
	/*
	float distSq = 0.f;

	float delta;
	delta = b0->pos.x - b1->pos.x;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	delta = b0->pos.y - b1->pos.y;
	if (delta < 0.f) // abs()
		delta *= -1.f;
	if (delta > 100.f - delta)
	{
		delta = 100.f - delta;
	}
	distSq += delta*delta;
	return sqrt(distSq);
	*/
}