#pragma once

#include <vector>
#include "line.h"
#include "rasterise.h"

class space;

class geometry
{
public:
	geometry(space* par);
	~geometry();

	space* pSpace = nullptr;

	std::vector<line> walls;
	std::vector<line> spawnBoxes;
	line attractorBox;

	vec2d attractor;

	void addSpawnBox(line coords);
};

