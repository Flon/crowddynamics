#include "gui.h"
#include "space.h"
#include "renderer.h"

gui::gui(space* par) : number_entry_N(sfg::Entry::Create()), number_entry_GridSize(sfg::Entry::Create()), number_entry_speed(sfg::Entry::Create()), number_entry_range(sfg::Entry::Create()), number_entry_noise(sfg::Entry::Create()), number_entry_dt(sfg::Entry::Create()), number_entry_tao(sfg::Entry::Create()), number_entry_k(sfg::Entry::Create())
{
	pSpace = par;
	window->SetTitle("Menu");
	//window->Add(button);
	desktop.Add(window);
	

	// populate input boxes with default values
	std::stringstream number;
	number << pSpace->cfg->N;
	number_entry_N->SetText(sf::String(number.str()));
	number.str(std::string()); // clear
	number << pSpace->cfg->gridSizeX;
	number_entry_GridSize->SetText(sf::String(number.str()));
	number.str(std::string()); // clear
	number << pSpace->cfg->speed;
	number_entry_speed->SetText(sf::String(number.str()));
	number.str(std::string()); // clear
	number << pSpace->cfg->range;
	number_entry_range->SetText(sf::String(number.str()));
	number.str(std::string()); // clear
	number << pSpace->cfg->noiseMagnitude;
	number_entry_noise->SetText(sf::String(number.str()));
	number.str(std::string()); //clear
	number << pSpace->cfg->dt;
	number_entry_dt->SetText(sf::String(number.str()));
	number.str(std::string()); //clear
	number << pSpace->cfg->tao;
	number_entry_tao->SetText(sf::String(number.str()));
	number.str(std::string()); //clear
	number << pSpace->cfg->K;
	number_entry_k->SetText(sf::String(number.str()));
	// toggle checkboxes
	if (pSpace->cfg->drawRadius)
		check_button_radius->SetActive(true);
	if (pSpace->cfg->drawDensity)
		check_button_density->SetActive(true);
	if (pSpace->cfg->drawDensitySquares)
		check_button_density_boxes->SetActive(true);
	if (pSpace->cfg->drawDebugGrid)
		check_button_grid->SetActive(true);
	if (pSpace->cfg->drawHelper)
		check_button_helper->SetActive(true);
	if (pSpace->cfg->drawBoxes)
		check_button_boxes->SetActive(true);
	if (pSpace->cfg->drawPressure)
		check_button_pressure->SetActive(true);

	// callback bindings
	button_snapshot->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&gui::onSnapshotButtonClick, this));
	button_reset_crowd->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&gui::onResetCrowdButtonClick, this));
	button_reset_geometry->GetSignal(sfg::Button::OnLeftClick).Connect(std::bind(&gui::onResetAllButtonClick, this));
	number_entry_N->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onNChanged, this));
	number_entry_GridSize->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onGridSizeChanged, this));
	number_entry_speed->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onSpeedChanged, this));
	number_entry_range->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onRangeChanged, this));
	number_entry_noise->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onNoiseChanged, this));
	number_entry_dt->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onDTChanged, this));
	number_entry_tao->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onTaoChanged, this));
	number_entry_k->GetSignal(sfg::Entry::OnTextChanged).Connect(std::bind(&gui::onKChanged, this));
	check_button_radius->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onRadiusToggled, this));
	check_button_density->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onDensityToggled, this));
	check_button_density_boxes->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onDensityBoxesToggled, this));
	check_button_grid->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onGridToggled, this));
	check_button_helper->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onHelperToggled, this));
	check_button_boxes->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onBoxesToggled, this));
	check_button_pressure->GetSignal(sfg::CheckButton::OnToggle).Connect(std::bind(&gui::onPressureToggled, this));

	// Simple styling 
	desktop.SetProperty("Window", "BackGroundColor", sf::Color(60, 60, 60));
	desktop.SetProperty("Window", "TitleBackgroundColor", sf::Color(60, 60, 60));
	desktop.SetProperty("Window", "BorderColor", sf::Color(60, 60, 60));

	// Layout.
	auto table = sfg::Table::Create();
	table->Attach(sfg::Label::Create("N:"), sf::Rect<sf::Uint32>(0, 0, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_N, sf::Rect<sf::Uint32>(1, 0, 1, 1));
	table->Attach(sfg::Label::Create("Grid:"), sf::Rect<sf::Uint32>(0, 1, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_GridSize, sf::Rect<sf::Uint32>(1, 1, 1, 1));
	table->Attach(sfg::Label::Create("Speed:"), sf::Rect<sf::Uint32>(0, 3, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_speed, sf::Rect<sf::Uint32>(1, 3, 1, 1));
	table->Attach(sfg::Label::Create("Range:"), sf::Rect<sf::Uint32>(0, 4, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_range, sf::Rect<sf::Uint32>(1, 4, 1, 1));
	table->Attach(sfg::Label::Create("Noise:"), sf::Rect<sf::Uint32>(0, 5, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_noise, sf::Rect<sf::Uint32>(1, 5, 1, 1));
	table->Attach(sfg::Label::Create("tao:"), sf::Rect<sf::Uint32>(0, 6, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_tao, sf::Rect<sf::Uint32>(1, 6, 1, 1));
	table->Attach(sfg::Label::Create("k:"), sf::Rect<sf::Uint32>(0, 7, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_k, sf::Rect<sf::Uint32>(1, 7, 1, 1));
	table->Attach(sfg::Label::Create("dt:"), sf::Rect<sf::Uint32>(0, 8, 1, 1), sfg::Table::FILL, sfg::Table::FILL);
	table->Attach(number_entry_dt, sf::Rect<sf::Uint32>(1, 8, 1, 1));
	// table->Attach(check_button_density, sf::Rect<sf::Uint32>(0, 9, 1, 1));

	table->SetColumnSpacings(2.f);
	table->SetRowSpacings(1.f);

	auto content_vbox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.f);
	content_vbox->Pack(sfg::Label::Create("    Parameters    "));
	content_vbox->Pack(table);
	content_vbox->Pack(sfg::Label::Create("      Render      "));
	content_vbox->Pack(check_button_radius);
	content_vbox->Pack(check_button_pressure);
	content_vbox->Pack(check_button_density);
	content_vbox->Pack(check_button_density_boxes);
	content_vbox->Pack(check_button_grid);
	content_vbox->Pack(check_button_helper);
	content_vbox->Pack(check_button_boxes);
	content_vbox->Pack(button_snapshot);
	content_vbox->Pack(sfg::Label::Create("      Reset       "));
	content_vbox->Pack(button_reset_crowd);
	content_vbox->Pack(button_reset_geometry);

	window->Add(content_vbox);
}


gui::~gui()
{
}

void gui::onSnapshotButtonClick()
{
	pSpace->recordSnapshot(5);
}

void gui::onResetCrowdButtonClick()
{
	pSpace->resetCrowd();
	pSpace->rendr->constructSprite();
}

void gui::onResetAllButtonClick()
{
	pSpace->resetAll();
	pSpace->rendr->constructSprite();
}

void gui::onNChanged()
{
	//int newN = (int)(number_entry_N->GetText);
	//pSpace->cfg->N = newN;
	unsigned int buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_N->GetText()));
	sstr >> buf_number;
	
	if (buf_number < 0 || buf_number > 1000000) {
		number_entry_N->SetText("1");
		buf_number = 0;
		//return;
	}

	pSpace->cfg->N = buf_number;
}

void gui::onGridSizeChanged()
{
	unsigned int buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_GridSize->GetText()));
	sstr >> buf_number;

	if (buf_number < 0 || buf_number > 1000000) {
		number_entry_GridSize->SetText("1");
		buf_number = 0;
		//return;
	}

	pSpace->cfg->gridSizeX = buf_number;
	pSpace->cfg->gridSizeY = buf_number;
}

void gui::onSpeedChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_speed->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_speed->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->speed = buf_number;
}

void gui::onRangeChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_range->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_range->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->range = buf_number;
}

void gui::onNoiseChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_noise->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_noise->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->noiseMagnitude = buf_number;
}

void gui::onDTChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_dt->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_dt->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->dt = buf_number;
}

void gui::onKChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_k->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_k->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->K = buf_number;
}

void gui::onTaoChanged()
{
	float buf_number(0);

	std::stringstream sstr(static_cast<std::string>(number_entry_tao->GetText()));
	sstr >> buf_number;

	if (buf_number < 0.f || buf_number > 1000.f) {
		number_entry_tao->SetText("1");
		buf_number = 0.0f;
		//return;
	}

	pSpace->cfg->tao = buf_number;
}

void gui::onDensityToggled()
{
	if (pSpace->cfg->drawDensity)
	{
		pSpace->cfg->drawDensity = false;
		pSpace->rendr->drawDensity = false;
	}
	else
	{
		pSpace->cfg->drawDensity = true;
		pSpace->rendr->drawDensity = true;
	}
}

void gui::onDensityBoxesToggled()
{
	if (pSpace->cfg->drawDensitySquares)
	{
		pSpace->cfg->drawDensitySquares = false;
		pSpace->rendr->drawDensitySquares = false;
	}
	else
	{
		pSpace->cfg->drawDensitySquares = true;
		pSpace->rendr->drawDensitySquares = true;
	}
}

void gui::onGridToggled()
{
	if (pSpace->cfg->drawDebugGrid)
	{
		pSpace->cfg->drawDebugGrid = false;
		pSpace->rendr->drawDebugGrid = false;
	}
	else
	{
		pSpace->cfg->drawDebugGrid = true;
		pSpace->rendr->drawDebugGrid = true;
	}
}

void gui::onRadiusToggled()
{
	if (pSpace->cfg->drawRadius)
	{
		pSpace->cfg->drawRadius = false;
		pSpace->rendr->drawRadius = false;
		pSpace->rendr->constructSprite();
	}
	else
	{
		pSpace->cfg->drawRadius = true;
		pSpace->rendr->drawRadius = true;
		pSpace->rendr->constructSprite();
	}
}

void gui::onHelperToggled()
{
	if (pSpace->cfg->drawHelper)
	{
		pSpace->cfg->drawHelper = false;
		pSpace->rendr->drawHelper = false;
	}
	else
	{
		pSpace->cfg->drawHelper = true;
		pSpace->rendr->drawHelper = true;
	}
}

void gui::onBoxesToggled()
{
	if (pSpace->cfg->drawBoxes)
	{
		pSpace->cfg->drawBoxes = false;
		pSpace->rendr->drawBoxes = false;
	}
	else
	{
		pSpace->cfg->drawBoxes = true;
		pSpace->rendr->drawBoxes = true;
	}
}

void gui::onPressureToggled()
{
	if (pSpace->cfg->drawPressure)
	{
		pSpace->cfg->drawPressure = false;
		pSpace->rendr->drawPressure = false;
	}
	else
	{
		pSpace->cfg->drawPressure = true;
		pSpace->rendr->drawPressure = true;
	}
}