#pragma once
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <memory>
class space;
class gui
{
public:
	gui(space* par);
	~gui();
	sfg::Desktop desktop;
	sfg::SFGUI sfgui;
private:
	space* pSpace = nullptr;
	sfg::Window::Ptr window = sfg::Window::Create();
	sfg::Button::Ptr button_reset_crowd = sfg::Button::Create("Crowd");
	sfg::Button::Ptr button_reset_geometry = sfg::Button::Create("Geometry");
	sfg::Button::Ptr button_snapshot = sfg::Button::Create("Snapshot");
	sfg::CheckButton::Ptr check_button_radius = sfg::CheckButton::Create("Radius");
	sfg::CheckButton::Ptr check_button_density = sfg::CheckButton::Create("Density");
	sfg::CheckButton::Ptr check_button_pressure = sfg::CheckButton::Create("Pressure");
	sfg::CheckButton::Ptr check_button_density_boxes = sfg::CheckButton::Create("Density Boxes");
	sfg::CheckButton::Ptr check_button_grid = sfg::CheckButton::Create("Grid");
	sfg::CheckButton::Ptr check_button_helper = sfg::CheckButton::Create("Helper");
	sfg::CheckButton::Ptr check_button_boxes = sfg::CheckButton::Create("Boxes");
	sfg::Entry::Ptr number_entry_N;
	sfg::Entry::Ptr number_entry_GridSize;
	sfg::Entry::Ptr number_entry_speed;
	sfg::Entry::Ptr number_entry_range;
	sfg::Entry::Ptr number_entry_noise;
	sfg::Entry::Ptr number_entry_dt;
	sfg::Entry::Ptr number_entry_tao;
	sfg::Entry::Ptr number_entry_k;
	void onSnapshotButtonClick();
	void onResetCrowdButtonClick();
	void onResetAllButtonClick();
	void onNChanged();
	void onGridSizeChanged();
	void onSpeedChanged();
	void onRangeChanged();
	void onNoiseChanged();
	void onDTChanged();
	void onTaoChanged();
	void onKChanged();
	void onDensityToggled();
	void onPressureToggled();
	void onDensityBoxesToggled();
	void onGridToggled();
	void onRadiusToggled();
	void onHelperToggled();
	void onBoxesToggled();
};

