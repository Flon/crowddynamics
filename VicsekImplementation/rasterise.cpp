#include "rasterise.h"
#include "flock2.h"
#include <algorithm>

//#include <stddef.h>
#include <limits>
//#include <stdlib.h>
//#include <stdio.h>
//#include <string.h>
//#include <time.h>

#include "vec2d.h"

//#define SCREEN_HEIGHT 22
//#define SCREEN_WIDTH  78

// Simulated frame buffer
/*
char Screen[SCREEN_HEIGHT][SCREEN_WIDTH];

void SetPixel(long x, long y, char color)
{
if ((x < 0) || (x >= SCREEN_WIDTH) ||
(y < 0) || (y >= SCREEN_HEIGHT))
{
return;
}

Screen[y][x] = color;
}
*/

rasteriser::rasteriser(flock2* par)
{
	pFlock = par;
	currentWall = 0;
}

void rasteriser::addGrid(int x, int y)
{
	if ((x < 0) || (x >= pFlock->gridSizeX) || //GridSize
		(y < 0) || (y >= pFlock->gridSizeY)) //GridSize
	{
		return;
	}

	//Screen[y][x] = color;
	// add Grid coordinates to list
	//int currentWall = pFlock->walls.size() - 1;
	std::vector<unsigned int> coordinate = { (unsigned int)x, (unsigned int)y };
	pFlock->wallIntersections[currentWall].push_back(coordinate);
}

void rasteriser::partitionWall(line wall)
{
	pFlock->wallIntersections.push_back(std::vector<std::vector<unsigned int>>(0, std::vector<unsigned int>(0)));
	// calculate vector bounds
	
	vec2d p0 = wall.A;
	vec2d p1 = wall.B;
	vec2d A, B, C, D;
	vec2d norm, orth;

	norm.x = p1.x - p0.x;
	norm.y = p1.y - p0.y;
	float mag = sqrt(norm.x*norm.x + norm.y*norm.y);
	norm.x /= mag;
	norm.y /= mag;

	orth.x = norm.y;
	orth.y = norm.x;

	float range = pFlock->range;

	A.x = p0.x - (norm.x - orth.x)*range*2.5f;
	A.y = p0.y - (norm.y + orth.y)*range*2.5f;

	B.x = p1.x + (norm.x - orth.x)*range*2.5f;
	B.y = p1.y + (norm.y + orth.y)*range*2.5f;

	C.x = p1.x + (norm.x + orth.x)*range*2.5f;
	C.y = p1.y + (norm.y - orth.y)*range*2.5f;

	D.x = p0.x - (norm.x + orth.x)*range*2.5f;
	D.y = p0.y - (norm.y - orth.y)*range*2.5f;
	// insert triangles ABC and CDA
	// debug draw triangles
	pFlock->debugTriangles.push_back(A);
	pFlock->debugTriangles.push_back(B);
	pFlock->debugTriangles.push_back(C);
	pFlock->debugTriangles.push_back(C);
	pFlock->debugTriangles.push_back(D);
	pFlock->debugTriangles.push_back(A);
	drawTriangle(A, B, C);
	drawTriangle(B, A, D);
	currentWall++;
}

void rasteriser::drawTriangle(vec2d p0, vec2d p1, vec2d p2)
{
	// convert vertex coords to grid coords
	p0.x *= pFlock->gridSizeX / 100.f;
	p0.y *= pFlock->gridSizeX / 100.f;
	p1.x *= pFlock->gridSizeX / 100.f;
	p1.y *= pFlock->gridSizeX / 100.f;
	p2.x *= pFlock->gridSizeX / 100.f;
	p2.y *= pFlock->gridSizeX / 100.f;
	// calculate boundary
	int maxX = (int)(std::max(p0.x, std::max(p1.x, p2.x)));
	int minX = (int)(std::min(p0.x, std::min(p1.x, p2.x)));
	int maxY = (int)(std::max(p0.y, std::max(p1.y, p2.y)));
	int minY = (int)(std::min(p0.y, std::min(p1.y, p2.y)));

	vec2d vs1, vs2;
	vs1.x = p1.x - p0.x;
	vs1.y = p1.y - p0.y;
	vs2.x = p2.x - p0.x;
	vs2.y = p2.y - p0.y;
	// loop through bounding box cell coordinates
	for (int x = minX; x <= maxX; x++)
	{
		for (int y = minY; y <= maxY; y++)
		{
			vec2d q;
			q.x = (float)x - p0.x + 0.5f;
			q.y = (float)y - p0.y + 0.5f;

			float s = crossProduct(q, vs2) / crossProduct(vs1, vs2);
			float t = crossProduct(vs1, q) / crossProduct(vs1, vs2);

			if ((s >= 0.f) && (t >= 0.f) && (s + t <= 1.f)) // test if inside triangle
			{
				addGrid(x, y);
			}
		}
	}
}

float rasteriser::crossProduct(vec2d a, vec2d b)
{
	float cmag = a.x*b.y - a.y*b.x;
	return cmag;
}

/*
int main(void)
{
Point2D p0, p1, p2;

// clear the screen
memset(Screen, ' ', sizeof(Screen));

// generate random trinagle coordinates
srand((unsigned)time(NULL));

p0.x = rand() % SCREEN_WIDTH;
p0.y = rand() % SCREEN_HEIGHT;

p1.x = rand() % SCREEN_WIDTH;
p1.y = rand() % SCREEN_HEIGHT;

p2.x = rand() % SCREEN_WIDTH;
p2.y = rand() % SCREEN_HEIGHT;

// draw the triangle
p0.color = '1';
DrawTriangle(p0, p1, p2);

// also draw the triangle's vertices
SetPixel(p0.x, p0.y, '*');
SetPixel(p1.x, p1.y, '*');
SetPixel(p2.x, p2.y, '*');


return 0;
}
*/

//};