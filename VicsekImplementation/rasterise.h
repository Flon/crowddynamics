#pragma once
#include "vec2d.h"
#include <vector>
#include "line.h"

typedef struct
{
	int x, y;
	//unsigned char color;
} point2d;

class flock2;

class rasteriser
{
public:
	rasteriser(flock2* par);
	~rasteriser();

	flock2* pFlock = nullptr;
	void addGrid(int x, int y);
	void drawTriangle(vec2d p0, vec2d p1, vec2d p2);
	float crossProduct(vec2d a, vec2d b);
	void partitionWall(line wall);
	int currentWall;
};

