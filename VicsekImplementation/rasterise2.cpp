#include "rasterise2.h"
namespace rasterise
{
	edge::edge(int x1, int y1, int x2, int y2)
	{
		if (y1 < y2) {
			X1 = x1;
			Y1 = y1;

			X2 = x2;
			Y2 = y2;
		}
		else {
			X1 = x2;
			Y1 = y2;

			X2 = x1;
			Y2 = y1;
		}
	}
}