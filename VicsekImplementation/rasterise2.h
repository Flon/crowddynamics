#pragma once
namespace rasterise
{
	class edge
	{
	public:
		float X1, Y1, X2, Y2;
		edge(int x1, int y1, int x2, int y2);
	};

	void
		drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3)
	{
		// create edges for the triangle
		edge edges[3] = {
			edge((int)x1, (int)y1, (int)x2, (int)y2),
			edge((int)x2, (int)y2, (int)x3, (int)y3),
			edge((int)x3, (int)y3, (int)x1, (int)y1)
		};
	}
}