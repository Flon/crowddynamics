#include "renderer.h"
#include "space.h"
#include "gui.h"
#include <math.h>

inline int clamp(int x, int a, int b)

{

	return x < a ? a : (x > b ? b : x);

}

renderer::renderer(space* par)
{
	pSpace = par;
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	pWindow = new sf::RenderWindow(sf::VideoMode(700, 700), "Flocking", sf::Style::Default, settings);
	drawTrace = pSpace->cfg->drawTrace;
	focusView = pSpace->cfg->focusView;
	drawRadius = pSpace->cfg->drawRadius;
	drawDebugGrid = pSpace->cfg->drawDebugGrid;
	drawDebugTris = pSpace->cfg->drawDebugTris;
	drawDensity = pSpace->cfg->drawDensity;
	drawDensitySquares = pSpace->cfg->drawDensitySquares;
	drawPressure = pSpace->cfg->drawPressure;
	drawBoxes = pSpace->cfg->drawBoxes;
	//pVerts = new sf::Vertex[pSpace->cfg->N];
	pView = new sf::View(sf::Vector2f(350, 350), sf::Vector2f(350, 350));
	pView->zoom(2.f);
	pWindow->setView(*pView);
	// construct the boid sprite
	constructSprite();

	//setup trace render target
	texTrace.create(700, 700);
	sprTrace.setTexture(texTrace.getTexture());
	sprTrace.setOrigin(sf::Vector2f(350.f, 350.f));
	sprTrace.setPosition(sf::Vector2f(350.f, 350.f));
	sprTrace.setScale(1.f, -1.f); //flip sprite
	//texTrace.clear(sf::Color::Black);
	//std::cout << (*pWindow).getSettings().antialiasingLevel;
}


renderer::~renderer()
{
	delete pWindow;
	delete pVerts;
	delete pView;
}

void renderer::render()
{
	// render
	pWindow->clear(sf::Color::White);
	std::vector<boid2>* pBoids = &(pSpace->flk->boids);
	// draw points
	/*
	for (unsigned int i = 0; i < (*pBoids).size(); i++)
	{
		pVerts[i].position = sf::Vector2f((*pBoids)[i].pos.x*7.f, (*pBoids)[i].pos.y*7.f);
	}
	pWindow->draw(pVerts, pSpace->cfg->N, sf::Points);
	*/

	// spawn boxes
	if (drawBoxes)
	{
		std::vector<line>& spawnBoxes = pSpace->geom->spawnBoxes;
		int numBoxes = spawnBoxes.size();
		sf::VertexArray boxes(sf::Quads, numBoxes * 4);
		for (int i = 0; i < numBoxes; i++)
		{

			boxes[4 * i] = sf::Vertex(sf::Vector2f(spawnBoxes[i].A.x*7.f, spawnBoxes[i].A.y*7.f), sf::Color(255, 255, 100, 128));
			boxes[4 * i + 1] = sf::Vertex(sf::Vector2f(spawnBoxes[i].A.x*7.f, spawnBoxes[i].B.y*7.f), sf::Color(255, 255, 100, 128));
			boxes[4 * i + 2] = sf::Vertex(sf::Vector2f(spawnBoxes[i].B.x*7.f, spawnBoxes[i].B.y*7.f), sf::Color(255, 255, 100, 128));
			boxes[4 * i + 3] = sf::Vertex(sf::Vector2f(spawnBoxes[i].B.x*7.f, spawnBoxes[i].A.y*7.f), sf::Color(255, 255, 100, 128));
		}
		pWindow->draw(boxes);
	}

	// attractor
	if (true)
	{
		vec2d& attractor = pSpace->geom->attractor;
		sf::CircleShape circle(10);
		circle.setOrigin(sf::Vector2f(10, 10));
		circle.setOutlineThickness(-4);
		circle.setOutlineColor(sf::Color(200, 255, 128));
		circle.setFillColor(sf::Color(100, 255, 64));
		circle.setPosition(sf::Vector2f(attractor.x*7.f, attractor.y*7.f));
		pWindow->draw(circle);
	}

	// attractorBox
	if (drawBoxes)
	{
		line& attractorBox = pSpace->geom->attractorBox;
		sf::VertexArray boxes(sf::Quads, 4);
		boxes[0] = sf::Vertex(sf::Vector2f(attractorBox.A.x*7.f, attractorBox.A.y*7.f), sf::Color(128, 128, 255, 80));
		boxes[1] = sf::Vertex(sf::Vector2f(attractorBox.A.x*7.f, attractorBox.B.y*7.f), sf::Color(128, 128, 255, 80));
		boxes[2] = sf::Vertex(sf::Vector2f(attractorBox.B.x*7.f, attractorBox.B.y*7.f), sf::Color(128, 128, 255, 80));
		boxes[3] = sf::Vertex(sf::Vector2f(attractorBox.B.x*7.f, attractorBox.A.y*7.f), sf::Color(128, 128, 255, 80));
		pWindow->draw(boxes);
	}

	
	// Density Map
	if (drawDensity)
	{
		float offset = 100.f*7.f / float(pSpace->flk->gridSizeX);
		int gsx = pSpace->flk->gridSizeX;
		int gsy = pSpace->flk->gridSizeY;
		sf::VertexArray densityVerts(sf::Quads, 4*gsx*gsy);
		for (int x = 0; x < gsx - 1; x++)
		{
			for (int y = 0; y < gsy - 1; y++)
			{
				int xGrid = x;
				int yGrid = y;
				float xCoord = ((float)xGrid + 0.5f)*offset;
				float yCoord = ((float)yGrid + 0.5f)*offset;
				int index = gsx*yGrid + xGrid;
				int quadIndex = 0+x*4+y*gsx*4; //x + y*WIDTH + Z*WIDTH*DEPTH
				int density = clamp(pSpace->flk->grid[index].size() * 20 - 20, 0, 255);
				densityVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), sf::Color(255, 0, 0, density));
				
				xGrid = x + 1;
				xCoord = ((float)xGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 1 + x * 4 + y*gsx*4;
				density = clamp(pSpace->flk->grid[index].size() * 20 - 20, 0, 255);
				densityVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), sf::Color(255, 0, 0, density));

				yGrid = y + 1;
				yCoord = ((float)yGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 2 + x * 4 + y*gsx*4;
				density = clamp(pSpace->flk->grid[index].size() * 20 - 20, 0, 255);
				densityVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), sf::Color(255, 0, 0, density));

				xGrid = x;
				xCoord = ((float)xGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 3 + x * 4 + y*gsx*4;
				density = clamp(pSpace->flk->grid[index].size() * 20 - 20, 0, 255);
				densityVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), sf::Color(255, 0, 0, density));
			}
		}
		pWindow->draw(densityVerts);
	}

	// pressure map
	if (drawPressure)
	{
		pSpace->flk->calcPressure();
		//std::cout << pSpace->flk->pressures[0];

		float offset = 100.f*7.f / float(pSpace->flk->gridSizeX);
		int gsx = pSpace->flk->gridSizeX;
		int gsy = pSpace->flk->gridSizeY;
		sf::VertexArray pressureVerts(sf::Quads, 4 * gsx*gsy);
		for (int x = 0; x < gsx - 1; x++)
		{
			for (int y = 0; y < gsy - 1; y++)
			{
				int xGrid = x;
				int yGrid = y;
				float xCoord = ((float)xGrid + 0.5f)*offset;
				float yCoord = ((float)yGrid + 0.5f)*offset;
				int index = gsx*yGrid + xGrid;
				int quadIndex = 0 + x * 4 + y*gsx * 4; //x + y*WIDTH + Z*WIDTH*DEPTH
				int pressure = clamp(pSpace->flk->pressures[index] * 5, 0, 255);
				pressureVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), colourGradient(pressure, 100.f));//sf::Color(0, 0, 255, pressure));

				xGrid = x + 1;
				xCoord = ((float)xGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 1 + x * 4 + y*gsx * 4;
				pressure = clamp(pSpace->flk->pressures[index] * 5, 0, 255);
				pressureVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), colourGradient(pressure, 100.f));

				yGrid = y + 1;
				yCoord = ((float)yGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 2 + x * 4 + y*gsx * 4;
				pressure = clamp(pSpace->flk->pressures[index] * 5, 0, 255);
				pressureVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), colourGradient(pressure, 100.f));

				xGrid = x;
				xCoord = ((float)xGrid + 0.5f)*offset;
				index = gsx*yGrid + xGrid;
				quadIndex = 3 + x * 4 + y*gsx * 4;
				pressure = clamp(pSpace->flk->pressures[index] * 5, 0, 255);
				pressureVerts[quadIndex] = sf::Vertex(sf::Vector2f(xCoord, yCoord), colourGradient(pressure, 100.f));
			}
		}
		pWindow->draw(pressureVerts);
	}


	// Draw density squares
	if (drawDensitySquares)
	{
		int gsx = pSpace->flk->gridSizeX;
		int gsy = pSpace->flk->gridSizeY;
		float offset = 100.f*7.f / float(gsx);
		sf::RectangleShape rectangle;
		rectangle.setSize(sf::Vector2f(offset, offset));

		for (int x = 0; x < gsx; x++)
		{
			for (int y = 0; y < gsy; y++)
			{
				int index = gsx*y + x;
				float density = clamp(pSpace->flk->grid[index].size() * 50 - 50, 0, 255);
				rectangle.setPosition(x*offset, y*offset);
				rectangle.setFillColor(sf::Color(255, 255 - density, 255 - density));
				pWindow->draw(rectangle);
			}
		}
	}

	//draw boids
	for (unsigned int i = 0; i < (*pBoids).size(); i++)
	{
		sprBoid.setPosition((*pBoids)[i].pos.x*7.f, (*pBoids)[i].pos.y*7.f);
		sprBoid.setRotation(atan2((*pBoids)[i].psi.y, (*pBoids)[i].psi.x) * 180.f / 3.14f);
		pWindow->draw(sprBoid);
	}

	// debug draw filled grid
	if (drawDebugGrid)
	{
		std::vector<std::vector<std::vector<unsigned int>>>& boxes = pSpace->flk->wallIntersections;
		float offset = 100.f*7.f / float(pSpace->flk->gridSizeX);
		sf::RectangleShape rectangle;
		rectangle.setSize(sf::Vector2f(offset, offset));
		rectangle.setOutlineColor(sf::Color::Green);
		rectangle.setOutlineThickness(-2);
		rectangle.setFillColor(sf::Color(0, 255, 150, 80));

		for (unsigned int i = 0; i < boxes.size(); i++)
		{
			for (unsigned int j = 0; j < boxes[i].size(); j++)
			{
				rectangle.setPosition(boxes[i][j][0] * offset, boxes[i][j][1] * offset);
				pWindow->draw(rectangle);
			}
		}
	}

	// debug triangles
	if (drawDebugTris)
	{
		sf::VertexArray dVerts(sf::Points, 1);
		std::vector < vec2d >& tris = pSpace->flk->debugTriangles;
		for (unsigned int i = 0; i < pSpace->flk->debugTriangles.size(); i++)
		{
			dVerts[0].color = sf::Color::Red;
			dVerts[0].position.x = tris[i].x*7.f;
			dVerts[0].position.y = tris[i].y*7.f;
			pWindow->draw(dVerts);
		}
	}
	if (drawWalls)
	{
		std::vector<line>& walls = pSpace->flk->walls;
		sf::VertexArray verts(sf::Lines, 2);
		for (unsigned int i = 0; i < walls.size(); i++)
		{
			verts[0].color = sf::Color::Blue;
			verts[1].color = sf::Color::Blue;
			verts[0].position.x = walls[i].A.x*7.f;
			verts[0].position.y = walls[i].A.y*7.f;
			verts[1].position.x = walls[i].B.x*7.f;
			verts[1].position.y = walls[i].B.y*7.f;
			pWindow->draw(verts);
		}
	}
	// move view
	if (focusView)
	{
		pView->setCenter(sf::Vector2f((*pBoids)[0].posPrev.x*7.f, (*pBoids)[0].posPrev.y*7.f));
		pWindow->setView(*pView);
	}
	// draw trace
	if (drawTrace)
	{
		trace[0].position = sf::Vector2f((*pBoids)[0].posPrev.x*7.f, (*pBoids)[0].posPrev.y*7.f);
		trace[1].position = sf::Vector2f((*pBoids)[0].pos.x*7.f, (*pBoids)[0].pos.y*7.f);
		trace[0].color = sf::Color::Red;
		trace[1].color = sf::Color::Red;
		// if boid hasnt wraped around draw the trace
		if ((pow(trace[0].position.x - trace[1].position.x, 2) < 25.f) && (pow(trace[0].position.y - trace[1].position.y, 2) < 25.f))
			texTrace.draw(trace, 2, sf::Lines);
		pWindow->draw(sprTrace);
	}
	// Draw Helper
	if (drawHelper)
	{
		sf::Vector2f pos;
		pos.x = float(sf::Mouse::getPosition(*pWindow).x);
		pos.y = float(sf::Mouse::getPosition(*pWindow).y);
		// transform to simulation coordinates
		pos.x /= 7.f;
		pos.y /= 7.f;

		// snap to grid
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		{
			int gsx = pSpace->flk->gridSizeX;
			float boxLength = float(100.f / gsx);
			pos.x = float(int(pos.x / boxLength)*boxLength) + boxLength / 2.f;// +offset;
			pos.y = float(int(pos.y / boxLength)*boxLength) + boxLength / 2.f;// +offset;
		}
		sf::VertexArray crossVerts(sf::Lines, 4);
		crossVerts[0].position = sf::Vector2f(pos.x*7.f, (pos.y + 100.f)*7.f);
		crossVerts[0].color = sf::Color(0, 255, 100, 160);
		crossVerts[1].position = sf::Vector2f(pos.x*7.f, (pos.y - 100.f)*7.f);
		crossVerts[1].color = sf::Color(0, 255, 100, 160);
		crossVerts[2].position = sf::Vector2f((pos.x + 100.f)*7.f, pos.y*7.f);
		crossVerts[2].color = sf::Color(0, 255, 180, 160);
		crossVerts[3].position = sf::Vector2f((pos.x - 100.f)*7.f, pos.y*7.f);
		crossVerts[3].color = sf::Color(0, 255, 100, 160);
		pWindow->draw(crossVerts);
	}

	//pWindow->draw(sprBoid);
	// test quads
	/*
	sf::VertexArray testQuad(sf::Quads, 16);
	testQuad[0] = sf::Vertex(sf::Vector2f(25, 25), sf::Color(255, 255, 255));
	testQuad[1] = sf::Vertex(sf::Vector2f(50, 25), sf::Color(255, 255, 255));
	testQuad[2] = sf::Vertex(sf::Vector2f(50, 50), sf::Color(255, 128, 128));
	testQuad[3] = sf::Vertex(sf::Vector2f(25, 50), sf::Color(255, 255, 255));

	testQuad[4] = sf::Vertex(sf::Vector2f(50, 25), sf::Color(255, 255, 255));
	testQuad[5] = sf::Vertex(sf::Vector2f(75, 25), sf::Color(255, 255, 255));
	testQuad[6] = sf::Vertex(sf::Vector2f(75, 50), sf::Color(255, 255, 255));
	testQuad[7] = sf::Vertex(sf::Vector2f(50, 50), sf::Color(255, 128, 128));

	testQuad[8] = sf::Vertex(sf::Vector2f(25, 50), sf::Color(255, 255, 255));
	testQuad[9] = sf::Vertex(sf::Vector2f(50, 50), sf::Color(255, 128, 128));
	testQuad[10] = sf::Vertex(sf::Vector2f(50, 75), sf::Color(255, 255, 255));
	testQuad[11] = sf::Vertex(sf::Vector2f(25, 75), sf::Color(255, 255, 255));

	testQuad[12] = sf::Vertex(sf::Vector2f(50, 50), sf::Color(255, 128, 128));
	testQuad[13] = sf::Vertex(sf::Vector2f(75, 50), sf::Color(255, 255, 255));
	testQuad[14] = sf::Vertex(sf::Vector2f(75, 75), sf::Color(255, 255, 255));
	testQuad[15] = sf::Vertex(sf::Vector2f(50, 75), sf::Color(255, 255, 255));
	pWindow->draw(testQuad);
	*/
	// draw gui
	pSpace->ui->desktop.Update(0.0f); //i->desktop.Update(1.0f);
	//render_window.clear();
	pSpace->ui->sfgui.Display(*pWindow);//sfgui.Display(*pWindow);
	//render_window.display();

	pWindow->display();
}

void renderer::constructSprite()
{
	texBoid.create(70, 70);
	sprBoid.setTexture(texBoid.getTexture());
	sprBoid.setOrigin(sf::Vector2f(35, 35));
	sprBoid.setPosition(sf::Vector2f(0.f, 0.f));
	sprBoid.setScale((pSpace->cfg->range * 2) / 10.f, -(pSpace->cfg->range * 2)/10.f); //flip sprite
	texBoid.clear(sf::Color(0, 0, 0, 0));
	texBoid.setSmooth(true);
	// draw line
	sf::RectangleShape line(sf::Vector2f(35, 12));
	line.setFillColor(sf::Color(0, 0, 0));
	line.setOutlineThickness(-2);
	line.setOutlineColor(sf::Color(60, 60, 60));
	line.setPosition(30, 30);
	texBoid.draw(line);
	if (drawRadius)
	{
		// draw circle outline
		sf::CircleShape shape(35);
		shape.setOutlineThickness(-12);
		shape.setOutlineColor(sf::Color(60, 60, 60));
		shape.setFillColor(sf::Color(0, 0, 0, 0));
		texBoid.draw(shape);
		// draw circle
		shape.setRadius(33);
		shape.setPosition(2, 2);
		shape.setOutlineThickness(-8);
		shape.setOutlineColor(sf::Color(0, 0, 0));
		texBoid.draw(shape);
		// draw overline
		line.setSize(sf::Vector2f(35, 8));
		line.setOutlineThickness(0);
		line.setPosition(32, 32);
		texBoid.draw(line);
	}
}

sf::Color renderer::colourGradient(float value, float maxValue)
{
	maxValue = 300.f;
	// std::max(lower, std::min(n, upper));
	float normValue = std::max(0.f, std::min(6.28f*value / maxValue, 6.28f));
	float red = std::max(0.f, sin(normValue + 3.14f)) * 255;
	float green = std::max(0.f, sin(normValue + 4.71f)) * 255;
	float blue = std::max(0.f, sin(normValue)) * 255;
	
	return sf::Color(red,green,blue,(red + green + blue) / 2.f);
}