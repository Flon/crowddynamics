#pragma once
#include <SFML/Graphics.hpp>
#include <array>
class space;

class renderer
{
public:
	renderer(space* par);
	~renderer();
	bool drawTrace;
	bool focusView;
	bool drawRadius;
	bool drawDebugGrid;
	bool drawDebugTris = false;
	bool drawDensity;
	bool drawDensitySquares;
	bool drawHelper;
	bool drawWalls = true;
	bool drawBoxes;
	bool drawPressure;
	space* pSpace = nullptr;
	sf::RenderWindow* pWindow = nullptr;
	sf::View* pView = nullptr;
	sf::RenderTexture texTrace;
	sf::RenderTexture texBoid;
	sf::Sprite sprBoid;
	sf::Sprite sprTrace;
	sf::Vertex* pVerts = nullptr;
	sf::Vertex trace[2];
	
	void calcPressure();

	void render();
	void constructSprite();

	sf::Color colourGradient(float value, float maxValue);
};

