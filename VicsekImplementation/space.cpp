#include "space.h"
#include "renderer.h"
#include "flock2.h"
#include "gui.h"

space::space()
{
	cfg = new config;
	rendr = new renderer(this);
	geom = new geometry(this);
	flk = new flock2(this, *geom);
	flk->randomise();
	stats = new statistics(this);
	ui = new gui(this);
}


space::~space()
{
	delete rendr;
	delete cfg;
	delete flk;
	delete stats;
	delete ui;
}

void space::run()
{
	timeSinceLastUpdate = sf::Time::Zero;
	TimePerFrame = sf::seconds(1.f / 60.f);
	while (rendr->pWindow->isOpen())
	{
		// Tick
		elapsedTime = timer.restart();
		timeSinceLastUpdate += elapsedTime;
		if (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;

			// processEvents(window);
			sf::Event event;
			while (rendr->pWindow->pollEvent(event))
			{
				// gui event handling
				ui->desktop.HandleEvent(event);

				if (event.type == sf::Event::Closed)
				{
					rendr->pWindow->close();
					return;
				}
				/*
				else if (event.type == sf::Event::KeyPressed)
				{

				}
				*/
			}
			handleUserInput();
			
			rendr->render();
		}
		flk->update();
	}
}

void space::resetCrowd()
{
	delete flk;
	flk = new flock2(this, *geom);
	flk->randomise();
}

void space::resetAll()
{
	delete geom;
	geom = new geometry(this);
	resetCrowd();
}

void space::handleUserInput()
{
	sf::Vector2i mousePos;
	sf::Vector2f pos;
	if (sf::Keyboard::isKeyPressed)
	{ 
		pos.x = float(sf::Mouse::getPosition(*rendr->pWindow).x);
		pos.y = float(sf::Mouse::getPosition(*rendr->pWindow).y);
		// transform to simulation coordinates
		pos.x /= 7.f;
		pos.y /= 7.f;

		// snap to grid
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		{
			int gsx = flk->gridSizeX;
			float boxLength = float(100.f / gsx);
			pos.x = float(int(pos.x / boxLength)*boxLength) + boxLength / 2.f;// +offset;
			pos.y = float(int(pos.y / boxLength)*boxLength) + boxLength / 2.f;// +offset;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !wDown)
	{
		wDown = true;
		if (wallStart)
		{
			wall.A.x = pos.x;
			wall.A.y = pos.y;

			wallStart = false;
		}
		else
		{
			wall.B.x = pos.x;
			wall.B.y = pos.y;
			flk->addWall(wall);

			wallStart = true;
		}
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		wDown = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::B) && !bDown)
	{
		bDown = true;

		if (boxStart)
		{
			box.A.x = pos.x;
			box.A.y = pos.y;

			boxStart = false;
		}
		else
		{
			box.B.x = pos.x;
			box.B.y = pos.y;
			// top wall
			wall.A.x = box.A.x;
			wall.A.y = box.A.y;
			wall.B.x = box.B.x;
			wall.B.y = box.A.y;
			flk->addWall(wall);
			// right wall
			wall.A.x = box.B.x;
			wall.B.x = box.B.x;
			wall.B.y = box.B.y;
			flk->addWall(wall);
			// bottom wall
			wall.A.x = box.B.x;
			wall.A.y = box.B.y;
			wall.B.x = box.A.x;
			wall.B.y = box.B.y;
			flk->addWall(wall);
			// left wall
			wall.A.x = box.A.x;
			wall.A.y = box.B.y;
			wall.B.x = box.A.x;
			wall.B.y = box.A.y;
			flk->addWall(wall);
			boxStart = true;
		}
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		bDown = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && !sDown)
	{
		sDown = true;

		if (spawnBoxStart)
		{
			wall.A.x = pos.x;
			wall.A.y = pos.y;

			spawnBoxStart = false;
		}
		else
		{
			wall.B.x = pos.x;
			wall.B.y = pos.y;
			geom->addSpawnBox(wall);

			spawnBoxStart = true;
		}
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		sDown = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !aDown)
	{
		aDown = true;
		geom->attractor.x = pos.x;
		geom->attractor.y = pos.y;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		aDown = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::V) && !vDown)
	{
		vDown = true;

		if (attractorBoxStart)
		{
			wall.A.x = pos.x;
			wall.A.y = pos.y;

			attractorBoxStart = false;
		}
		else
		{
			wall.B.x = pos.x;
			wall.B.y = pos.y;
			geom->attractorBox=wall;

			attractorBoxStart = true;
		}
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		vDown = false;
}

void space::recordSnapshot(int steps)
{
	resetCrowd();

	std::ofstream statfile;
	statfile.open("example.txt");
	int numValues = flk->pressures.size();

	for (int i = 0; i < steps; i++)
	{
		// advance one step
		flk->update();
		flk->calcPressure();
		// record values
		for (int j = 0; j < numValues; j++)
		{
			statfile << flk->pressures[j] << " ";
		}
		statfile << "\n";
	}
	statfile.close();
}