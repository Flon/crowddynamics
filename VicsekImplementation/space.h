#pragma once
#include "flock2.h"
#include "config.h"
#include "statistics.h"
#include <SFML/System.hpp>
#include <fstream>
#include <iostream>

class renderer;
class flock2;
class geometry;
class gui;

class space
{
public:
	space();
	~space();
	flock2* flk = nullptr;
	config* cfg = nullptr;
	renderer* rendr = nullptr;
	statistics* stats = nullptr;
	gui* ui = nullptr;
	geometry* geom = nullptr;
	sf::Clock timer;
	sf::Time timeSinceLastUpdate;
	sf::Time TimePerFrame;
	sf::Time elapsedTime;

	void run();
	void update();
	void resetCrowd();
	void resetAll();
	void handleUserInput();

	void recordSnapshot(int steps);
private:
	line wall;
	line box;
	bool wallStart = true;
	bool wDown = false;
	bool boxStart = true;
	bool bDown = false;
	bool spawnBoxStart = true;
	bool sDown = false;
	bool aDown = false;
	bool vDown = false;
	bool attractorBoxStart = true;
};

