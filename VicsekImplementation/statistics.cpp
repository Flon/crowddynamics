#include "statistics.h"
#include "space.h"

statistics::statistics(space* par)
{
	pSpace = par;
}


statistics::~statistics()
{
}

float statistics::calcOrderParam()
{
	std::vector<boid2>* pBoids = &(pSpace->flk->boids);
	float orderParam = 0.f;
	float x = 0.f;
	float y = 0.f;
	for (unsigned int i = 0; i < pSpace->flk->N; i++)
	{
		x += (*pBoids)[i].psi.x;
		y += (*pBoids)[i].psi.y;
	}
	orderParam = pow(x*x + y*y, 0.5f) / (float)(pSpace->cfg->N);

	return orderParam;
}